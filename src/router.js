import Vue from 'vue'
import Router from 'vue-router'
import CatalogoP from '@/components/Productos/Catalogo.vue'
import PapeleraP from '@/components/Productos/Papelera.vue'
import Home from '@/components/Home.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/catalogo',
            name: 'CatalogoP',
            component: CatalogoP
        },
        {
            path: '/papelera',
            name: 'Papelerap',
            component: PapeleraP
        }
    ]
})
